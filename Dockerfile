FROM debian:bullseye

USER root
RUN apt update && apt-get install postgresql-client -y
RUN useradd postgres --uid 999 -r -M -s /bin/bash
COPY duplicate.sh /usr/local/bin/dublicate.sh
WORKDIR /usr/local/bin/
RUN chown postgres:postgres dublicate.sh && chmod +x dublicate.sh
WORKDIR /
RUN ln -s usr/local/bin/dublicate.sh /
USER postgres

ENTRYPOINT ["dublicate.sh"]
