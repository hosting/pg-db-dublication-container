#!/bin/bash
#
#dublicate database from running database
#written for dublication of postgres database on kubernetes
#postgres managed by postgres-operator from zalando
#https://opensource.zalando.com/postgres-operator/
#script by dhof - fairkom.eu

#make sure script fails if one of the tasks fail
set -euo pipefail
IFS=$'\n\t'

#print env variables for debug
printenv

#check if ENV variables for shell script are set see https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash

if [ -z ${SRC_DBNAME+x} ]; then 
  echo "source database variable is unset";
  exit 1; 
else 
  echo "source database variable is set to '$SRC_DBNAME'"; 
fi

if [ -z ${TARGET_DBNAME+x} ]; then 
  echo "target database variable is unset"; 
  exit 1;
else 
  echo "target database variable is set to '$TARGET_DBNAME'"; 
fi

#check if sourcedb exists if -n ... if string is not null then ... else ...
if [[ -n `psql -Atqc "\list $SRC_DBNAME;"` ]]; then

   echo "Database $SRC_DBNAME exists"
   #check if targetdb exists - if -z ... if string is null then ... else ...
   if [[ -z `psql -Atqc "\list $TARGET_DBNAME;"` ]]; then
     #disallow all connections to src_database
     psql -c "UPDATE pg_database SET datallowconn=false WHERE datname='$SRC_DBNAME';"
     #stop all connections to src_database
     psql -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '$SRC_DBNAME' AND pid <> pg_backend_pid();" 
     #dublicate db postgres to db keycloak
     psql -c "CREATE DATABASE $TARGET_DBNAME TEMPLATE $SRC_DBNAME;"
     #show nice message if everything went well
     echo "Dublication of $SRC_DBNAME to $TARGET_DBNAME was successfull."
     #allow connections to postgres database
     psql -c "UPDATE pg_database SET datallowconn=true WHERE datname='$SRC_DBNAME';"
     #test connections to postgres and keycloak database
     echo "testing connection to source database $SRC_DBNAME"
     until psql -d $SRC_DBNAME
     do 
        echo "$SRC_DBNAME is not ready to get connections"
        sleep 1
     done
     echo "testing connection to target database $TARGET_DBNAME"
     until psql -d $TARGET_DBNAME
     do
       echo "$TARGET_DBNAME is not ready to get connections"
       sleep 1
     done
   else
     echo "Database $TARGET_DBNAME already exists."
   fi
else
  echo "Database $SRC_DBNAME doesn't exist."
fi

#TODO 
#- cleanup src db
#- maybe set user for target db; permissions?
