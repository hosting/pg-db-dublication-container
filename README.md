# pg-db-dublication-container

Dockerfile with a shell script that uses psql commands to dublicate a database with open connections.

Based on postgres-10 for all environment variables see (postgres documentation)[https://www.postgresql.org/docs/10/libpq-envars.html]

Basic ENV are in the docker-compose.yml file.

Additional ENV are:
- SRC_DBNAME ... Database to be dublicated
- TARGET_DBNAME ... Name of the target Database

## Usage

docker build -t pg-dublication .

docker-compose up

See dublication working - adjust SRC_DBNAME and TARGET_DBNAME to see what happens if either db doesn't exist.

## Todo
- cleanup of src-database
- make connection test working!!! - seems to be fixed with psql instead of pg_isrunning

## Steps needed to make it work with spilo

on leader cluster member patronictl pause.
After running the script run patronictl resume on master.
Check with patronictl list if any restarts on cluster members is needed.